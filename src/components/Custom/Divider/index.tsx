import { Divider, styled } from '@mui/material';

export const CustomizedDivider = styled(Divider)(() => ({
  '&.MuiDivider-root': {
    '&::before': {
      borderTop: 'thin solid #9F5026',
    },
    '&::after': {
      borderTop: 'thin solid #9F5026',
    },
    marginBottom: '20px',
  },
  '& .MuiDivider-wrapper': {
    fontSize: 16,
  },
})) as typeof Divider;
