import type { LinkProps } from '@mui/material';
import { Link } from '@mui/material';
import { styled } from '@mui/styles';
import NextJSLink from 'next/link';

export const NextLink = NextJSLink;

const UnlineLink = styled(Link)(() => ({
  borderWidth: 0,
})) as typeof Link;

type CustomLinkProps = LinkProps;

export const CustomLink: React.FC<CustomLinkProps> = ({ href, children }) => {
  return (
    <UnlineLink className="no-underline" href={href}>
      {children}
    </UnlineLink>
  );
};
