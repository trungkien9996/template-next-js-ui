import styled from '@emotion/styled';
import { Box } from '@mui/system';

export const BoxJustifyCenter = styled(Box)(() => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
})) as typeof Box;

export const BoxJustifyRight = styled(Box)(() => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'right',
})) as typeof Box;
