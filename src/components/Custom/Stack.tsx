import { Stack, styled } from '@mui/material';

export const StackSpaceBetween = styled(Stack)(() => ({
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
}));
