import { Grid } from '@mui/material';
import { Container } from '@mui/system';
import React from 'react';

import type { NextPageWithLayout } from '@/types/components.type';

const Page: NextPageWithLayout = () => {
  return (
    <>
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={12}></Grid>
          <Grid item xs={12}></Grid>
        </Grid>
      </Container>
    </>
  );
};

export default Page;
