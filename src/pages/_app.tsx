import '../styles/global.css';

import { Close } from '@mui/icons-material';
import { Box } from '@mui/system';
import type { DehydratedState } from '@tanstack/react-query';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import type { AppProps } from 'next/app';
import Head from 'next/head';
import type { SnackbarKey } from 'notistack';
import { SnackbarProvider } from 'notistack';
import React, { useState } from 'react';

import { ScrollToTop } from '@/components/ScrollToTop';
import { UserPageLayout } from '@/layouts/UserPageLayout';
import type { NextPageWithLayout } from '@/types/components.type';

export type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
  dehydratedState: DehydratedState;
};

const notistackRef = React.createRef<any>();

const onClickDismiss = (key: SnackbarKey) => () => {
  notistackRef.current.closeSnackbar(key);
};

const MyApp = (props: AppPropsWithLayout) => {
  const { Component, pageProps } = props;

  const [queryClient] = useState(() => new QueryClient());

  const Layout = Component.layout || UserPageLayout;

  return (
    <>
      <QueryClientProvider client={queryClient}>
        <Head>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <Box id="top-anchor" />
        <ScrollToTop />
        <SnackbarProvider
          autoHideDuration={3000}
          maxSnack={3}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          ref={notistackRef}
          action={(key) => <Close onClick={onClickDismiss(key)}></Close>}
        >
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </SnackbarProvider>
      </QueryClientProvider>
    </>
  );
};

export default MyApp;
