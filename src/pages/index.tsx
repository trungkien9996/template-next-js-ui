import { Box, Grid } from '@mui/material';
import Head from 'next/head';
import React from 'react';

import { UserPageLayout } from '@/layouts/UserPageLayout';
import { Messages, setMessage } from '@/lib/mesage';
import type { NextPageWithLayout } from '@/types/components.type';

const Page: NextPageWithLayout = () => {
  return (
    <>
      <Head>
        <title>{setMessage(Messages.app.shortTitle)}</title>
      </Head>
      <Box>
        <Grid container alignItems={'center'} spacing={3}>
          <Grid item xs={12}>
            container
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

Page.layout = UserPageLayout;

export default Page;
