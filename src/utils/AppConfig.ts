export const AppConfig = {
  site_name: 'Starter',
  title: 'Nextjs Starter',
  description: 'Starter code for your Nextjs Boilerplate with Tailwind CSS',
  locale: 'en',
};

export const LAYOUT_CONFIG = {
  main: {
    header: {
      first: {
        height: 15,
        mobileHeight: 15,
      },
      second: {
        height: 70,
        mobileHeight: 60,
      },
    },
  },
};
