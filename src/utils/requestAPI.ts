import type {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios';
import axios from 'axios';

import cookieService from './services/cookieService';

class RequestAPI {
  private readonly requestAPI: AxiosInstance;

  accessToken: string;

  constructor(public readonly apiUrl: string) {
    this.accessToken = cookieService.getAccessToken();
    this.requestAPI = axios.create({
      baseURL: this.apiUrl,
      withCredentials: false,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.accessToken}`,
      },
    });
  }

  async get<R = any>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.requestAPI
      .get(url, config)
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  async post<R = any>(
    url: string,
    data: Record<string, any>,
    config?: AxiosRequestConfig
  ): Promise<R> {
    return this.requestAPI
      .post(url, data, config)
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  async put<R = any>(
    url: string,
    data: Record<string, any>,
    config?: AxiosRequestConfig
  ): Promise<R> {
    return this.requestAPI
      .put(url, data, config)
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  async patch<R = any>(
    url: string,
    data: Record<string, any>,
    config?: AxiosRequestConfig
  ): Promise<R> {
    return this.requestAPI
      .patch(url, data, config)
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  async delete<R = any>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.requestAPI
      .delete(url, config)
      .then(this.handleResponse)
      .catch(this.handleError);
  }

  // eslint-disable-next-line class-methods-use-this
  handleResponse(response: AxiosResponse) {
    return response.data;
  }

  // eslint-disable-next-line class-methods-use-this
  handleError(error: AxiosError) {
    // eslint-disable-next-line @typescript-eslint/no-throw-literal
    if (error.response && error.response.data) throw error.response.data;

    if (error.request) throw error.request;
  }
}

export const requestAPI = new RequestAPI('http://localhost:4000' || '/');
