import type {
  Components,
  Mixins,
  Palette,
  Shadows,
  Transitions,
  ZIndex,
} from '@mui/material';
import { createTheme } from '@mui/material';
import type { Typography } from '@mui/material/styles/createTypography';
import type { Spacing } from '@mui/system';
import { createStyled } from '@mui/system';
import type { Theme as SystemTheme } from '@mui/system/createTheme';

import type { CustomPalette } from './palette';
// import type { CustomShadows } from './shadows';
// import shadows from './shadows';
import type { CustomShape } from './shape';
import shape from './shape';
import type { CustomTypography } from './typography';
import typography from './typography';

interface BaseTheme extends SystemTheme {
  mixins: Mixins;
  palette: Palette;
  shadows: Shadows;
  transitions: Transitions;
  typography: Typography;
  zIndex: ZIndex;
  unstable_strictMode?: boolean;
}

export type CustomTheme = {
  palette: CustomPalette;
  shape: CustomShape;
  // customShadows: CustomShadows;
  customShape: CustomShape;
  spacing: Spacing;
  typography: CustomTypography;
  components?: Components<BaseTheme>;
};

export const theme = createTheme({
  // palette,
  shape,
  // shadows,
  // customShadows,
  // customShape,
  typography,
});

export const styled = createStyled({ defaultTheme: theme });

// theme.components = com
