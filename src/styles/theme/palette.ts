import { alpha } from '@mui/material/styles';

import { colors } from './colors';

// SETUP COLORS
const GREY = {
  0: '#FFFFFF',
  100: '#F9FAFB',
  200: '#F4F6F8',
  300: '#DFE3E8',
  400: '#C4CDD5',
  500: '#919EAB',
  600: '#637381',
  700: '#454F5B',
  800: '#212B36',
  900: '#161C24',
  500_8: alpha('#919EAB', 0.08),
  500_12: alpha('#919EAB', 0.12),
  500_16: alpha('#919EAB', 0.16),
  500_24: alpha('#919EAB', 0.24),
  500_32: alpha('#919EAB', 0.32),
  500_48: alpha('#919EAB', 0.48),
  500_56: alpha('#919EAB', 0.56),
  500_80: alpha('#919EAB', 0.8),
};

const palette = {
  common: { black: colors.darkPaper, white: '#fff' },
  primary: {
    light: colors.primaryLight,
    main: colors.primaryMain,
    dark: colors.primaryDark,
    200: colors?.primary200,
    800: colors?.primary800,
    // darker: '#005249',
    contrast: '#fff',
  },
  secondary: {
    light: colors?.secondaryLight,
    main: colors?.secondaryMain,
    dark: colors?.secondaryDark,
    200: colors?.secondary200,
    800: colors?.secondary800,
    contrastText: '#fff',
  },
  price: { primary: '#e8121b', secondary: 'black', third: '#ee4d2d' },
  info: {
    main: '#1890FF',
  },
  success: {
    light: colors.successLight,
    main: colors.successMain,
    dark: colors?.warningDark,
    200: colors?.success200,
  },
  warning: {
    light: colors?.warningLight,
    main: colors?.warningMain,
    dark: colors?.warningDark,
  },
  error: {
    light: colors.errorLight,
    main: colors.errorMain,
    dark: colors.errorDark,
  },
  grey: {
    100: '#F9FAFB',
    200: '#F4F6F8',
    300: '#DFE3E8',
    400: '#C4CDD5',
    500: '#919EAB',
    600: '#637381',
    700: '#454F5B',
    800: '#212B36',
    900: '#161C24',
    500_8: alpha('#919EAB', 0.08),
    500_12: alpha('#919EAB', 0.12),
    500_16: alpha('#919EAB', 0.16),
    500_24: alpha('#919EAB', 0.24),
    500_32: alpha('#919EAB', 0.32),
    500_48: alpha('#919EAB', 0.48),
    500_56: alpha('#919EAB', 0.56),
    500_80: alpha('#919EAB', 0.8),
  },
  dark: {
    light: colors?.darkTextPrimary,
    main: colors?.darkLevel1,
    dark: colors?.darkLevel2,
    800: colors?.darkBackground,
    900: colors?.darkPaper,
  },
  divider: alpha('#919EAB', 0.24),
  text: {
    primary: colors.grey700,
    secondary: colors.grey500,
    disabled: colors.grey300,
  },
  background: {
    paper: colors.paper,
    default: colors.paper,
    primary: colors.darkBackground,
    secondary: colors.grey200,
  },
  action: {
    active: GREY[600],
    hover: GREY[500_8],
    selected: GREY[500_16],
    disabled: GREY[500_80],
    disabledBackground: GREY[500_24],
    focus: GREY[500_24],
    hoverOpacity: 0.08,
    disabledOpacity: 0.48,
  },
};

export type CustomPalette = typeof palette;

export default palette;

console.log();
