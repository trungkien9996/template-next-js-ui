import { Stack } from '@mui/material';
import Head from 'next/head';
import React from 'react';

import { UserContent } from '../Common/UserContent';
import { UserPageFooter } from './UserPageFooter';

interface IUserPageLayoutProps {
  children?: React.ReactNode;
}

export const UserPageLayout: React.FC<IUserPageLayoutProps> = ({
  children,
}) => {
  return (
    <>
      <Head>
        <title>Name</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Stack className="min-h-[100vh] overflow-hidden">
        Hearder
        <UserContent>{children}</UserContent>
        <UserPageFooter />
      </Stack>
    </>
  );
};
