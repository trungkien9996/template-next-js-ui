import { Container, Grid } from '@mui/material';
import type { FC } from 'react';

import { styled } from '../../styles/theme';

interface UserContentProps {
  children?: React.ReactNode;
}

export const UserContent = styled<FC<UserContentProps>>(
  ({ children, ...gridProps }) => (
    <Container>
      <Grid {...gridProps}>{children}</Grid>
    </Container>
  )
)(({ theme }) => ({
  flexGrow: 1,
  overflow: 'auto',
  minHeight: '100%',
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('lg')]: {
    padding: theme.spacing(2, 0),
  },
}));

interface NoMarginContentProps {
  children?: React.ReactNode;
}

export const NoMarginContent: FC<NoMarginContentProps> = ({
  children,
  ...gridProps
}) => (
  <Container>
    <Grid {...gridProps}>{children}</Grid>
  </Container>
);
