import { Box, Container, Typography } from '@mui/material';

export const FirstFooter = () => {
  return (
    <Box sx={{ backgroundColor: '#304d6c' }}>
      <Container>
        <Box pt={2} pb={2}>
          <Typography
            variant="body2"
            sx={{ color: 'white', textAlign: 'center' }}
          >
            @- Bản quyền thuộc về{' '}
          </Typography>
        </Box>
      </Container>
    </Box>
  );
};
