import React from 'react';

interface NoneLayoutProps {
  children?: React.ReactNode;
}

export const NoneLayout: React.FC<NoneLayoutProps> = ({ children }) => {
  return <>{children}</>;
};
