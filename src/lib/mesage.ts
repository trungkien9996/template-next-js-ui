export const Messages = {
  action: {
    accept: 'đồng ý',
    active: 'hoạt động',
    add: 'thêm',
    addTo: 'thêm vào',
    agree: 'đồng ý',
    alreadyHave: 'đã có',
    banned: 'bị khoá',
    become: 'trở thành',
    cancel: 'huỷ',
    change: 'thay đổi',
    confirm: 'xác nhận',
    continue: 'tiếp tục',
    create: 'tạo mới',
    delete: 'xoá',
    deleted: 'đã xoá',
    download: 'tải về',
    edit: 'sửa',
    enter: 'nhập',
    find: 'tìm',
    forgot: 'quên',
    forgotPassword: 'quên mật khẩu',
    found: 'tìm thấy',
    loadMore: 'tải nữa',
    login: 'đăng nhập',
    logout: 'đăng xuất',
    manage: 'quản lý',
    name: 'hành động',
    order: 'đặt hàng',
    pay: 'thanh toán',
    rate: 'đánh giá',
    register: 'đăng ký',
    registerNow: 'đăng ký ngay',
    remember: 'ghi nhớ',
    remove: 'xoá',
    review: 'đánh giá',
    reset: 'đặt lại',
    search: 'tìm kiếm',
    select: 'chọn',
    selectAll: 'chọn tất cả',
    seeMore: 'xem thêm',
    send: 'gửi',
    signIn: 'đăng nhập',
    sort: 'sắp xếp',
    update: 'cập nhật',
    upload: 'tải lên',
    verified: 'đã xác nhận',
    verify: 'xác nhận',
  },
  replacements: {
    require: 'yêu cầu nhập {document}',
    requireValid: 'yêu cầu nhập {document} hợp lệ',
    requireMaxLength: '{name} dài tối đa {length} ký tự',
    requireMaxValue: '{name} nhỏ hơn {value}',
    requireMinLength: '{name} dài tối thiểu {length} ký tự',
    requireMinValue: '{name} lớn hơn {value}',
    success: '{action} {document} thành công',
  },
  app: { shortTitle: 'template' },
};

export const setMessage = (...texts: (string | undefined)[]) => {
  let messages = '';

  if (texts.length) {
    for (let i = 0; i < texts.length; i += 1) {
      const text = texts[i];

      if (text) {
        if (i !== 0) {
          messages += ' ';
        }

        if (typeof text !== 'string') {
          messages += (text as any).toString();
        } else {
          messages += text;
        }
      }
    }
  }

  return messages.charAt(0).toUpperCase() + messages.slice(1);
};

export const setSuccessMessage = (
  action: string,
  document?: string
): string => {
  return setMessage(
    Messages.replacements.success
      .replace('{action}', action || '')
      .replace('{document}', document || '')
  );
};

export const setRequiredMaxLengthMessage = (length: number, name?: string) => {
  return setMessage(
    Messages.replacements.requireMaxLength
      .replace('{length}', `${length}`)
      .replace('{name}', name || 'tài liệu')
  );
};

export const setRequiredMinLengthMessage = (length: number, name?: string) => {
  return setMessage(
    Messages.replacements.requireMinLength
      .replace('{length}', `${length}`)
      .replace('{name}', name || 'tài liệu')
  );
};

export const setRequiredMessage = (document?: string) => {
  return setMessage(
    Messages.replacements.require.replace('{document}', document || '')
  );
};

export const setRequiredValidMessage = (document?: string) => {
  return setMessage(
    Messages.replacements.requireValid.replace('{document}', document || '')
  );
};

export const setRequiredMinValueMessage = (value: number, name?: string) => {
  return setMessage(
    Messages.replacements.requireMinValue
      .replace('{value}', `${value}`)
      .replace('{name}', name || 'tài liệu')
  );
};

export const setRequiredMaxValueMessage = (value: number, name?: string) => {
  return setMessage(
    Messages.replacements.requireMaxValue
      .replace('{value}', `${value}`)
      .replace('{name}', name || 'tài liệu')
  );
};

export const setReplaceMessage = (
  rawString: string,
  replacements: Record<string, string>
) => {
  const keys = Object.keys(replacements);

  let replacedString = rawString;

  for (const key of keys) {
    const tmp = replacements[key] || '';
    replacedString = replacedString.replace(`{${key}}`, tmp);
  }

  return setMessage(replacedString);
};

export const setReplaceOneMessage = (text: string, replacement: string) => {
  return setMessage(setReplaceMessage(text, { document: replacement }));
};

export const FullMessages = {
  accept: setMessage(Messages.action.accept),
  cancel: setMessage(Messages.action.cancel),
  create: setMessage(Messages.action.create),
  edit: setMessage(Messages.action.edit),
  reset: setMessage(Messages.action.reset),
  update: setMessage(Messages.action.update),
};

class MessagesService {
  setPageTitle(title?: string) {
    return `${setMessage(title || '')} - ${setMessage(
      Messages.app.shortTitle
    )}`;
  }
}

export const appShortTitle = setMessage(Messages.app.shortTitle);

export const messagesService = new MessagesService();
