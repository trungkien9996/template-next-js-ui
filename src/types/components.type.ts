import type { NextPage } from 'next';
import type { FC } from 'react';

export type NextPageWithLayout = NextPage & {
  layout?: FC;
};
