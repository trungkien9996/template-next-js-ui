import type { ERole } from './enum';

export interface IJwtPayload {
  username: string;
  id: string;
  iat: number;
  exp: number;
  role: ERole;
}
