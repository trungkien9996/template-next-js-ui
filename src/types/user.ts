export interface ILoginUser {
  username: string;
  password: string;
  remember: boolean;
}
